
package ar.com.controlclientes.data;

import ar.com.controlclientes.domain.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ClienteDaoImpl implements ClienteDao{
    
    @PersistenceContext(unitName = "ClientePU")
    EntityManager em;

    @Override
    public List<Cliente> readClientes() {
        return em.createNamedQuery("Cliente.encontrarTodosClientes").getResultList();
    }

    @Override
    public Cliente readCliente(Cliente cliente) {
        return em.find(Cliente.class, cliente.getIdCliente());
    }

    @Override
    public void createCliente(Cliente cliente) {
        em.persist(cliente);
        em.flush();
    }

    @Override
    public void updateCliente(Cliente cliente) {
        em.merge(cliente);
    }

    @Override
    public void deleteCliente(Cliente cliente) {
        em.remove(em.merge(cliente));
    }
    
}
