
package ar.com.controlclientes.data;

import ar.com.controlclientes.domain.Cliente;
import java.util.List;


public interface ClienteDao {
    
    public List<Cliente> readClientes();
    
    public Cliente readCliente(Cliente cliente);
    
    public void createCliente(Cliente cliente);
    
    public void updateCliente(Cliente cliente);
    
    public void deleteCliente(Cliente cliente);
    
}
