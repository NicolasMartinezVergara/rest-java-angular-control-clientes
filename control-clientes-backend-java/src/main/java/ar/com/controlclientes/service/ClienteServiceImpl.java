
package ar.com.controlclientes.service;

import ar.com.controlclientes.data.ClienteDao;
import ar.com.controlclientes.domain.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ClienteServiceImpl implements ClienteService{
    
    @Inject
    private ClienteDao clienteDao;

    @Override
    public List<Cliente> listarClientes() {
        return clienteDao.readClientes();
    }

    @Override
    public Cliente encontrarCliente(Cliente cliente) {
        return clienteDao.readCliente(cliente);
    }

    @Override
    public void agregarCliente(Cliente cliente) {
        clienteDao.createCliente(cliente);
    }

    @Override
    public void modificarCliente(Cliente cliente) {
        clienteDao.updateCliente(cliente);
    }

    @Override
    public void eliminarCliente(Cliente cliente) {
        clienteDao.deleteCliente(cliente);
    }
    
}
