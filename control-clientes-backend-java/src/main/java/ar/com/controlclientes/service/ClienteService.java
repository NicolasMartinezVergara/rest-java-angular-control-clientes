
package ar.com.controlclientes.service;

import ar.com.controlclientes.domain.Cliente;
import java.util.List;


public interface ClienteService {
    
    public List<Cliente> listarClientes();
    
    public Cliente encontrarCliente(Cliente cliente);
    
    public void agregarCliente(Cliente cliente);
    
    public void modificarCliente(Cliente cliente);
    
    public void eliminarCliente(Cliente cliente);
    
}
