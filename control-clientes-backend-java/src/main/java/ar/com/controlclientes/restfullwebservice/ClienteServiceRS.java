package ar.com.controlclientes.restfullwebservice;

import ar.com.controlclientes.domain.Cliente;
import ar.com.controlclientes.service.ClienteService;
import javax.ws.rs.core.*;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response.Status;



@Stateless
@Path("/clientes")
public class ClienteServiceRS {

    @Inject
    private ClienteService clienteService;

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Cliente> listarClientes() {
        List<Cliente> clientes = clienteService.listarClientes();
        System.out.println("Clientes encontrados " + clientes);
        return clientes;

    }

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Cliente encontrarCliente(@PathParam("id") int id) {
        Cliente cliente = clienteService.encontrarCliente(new Cliente(id));
        System.out.println("Cliente encontrado: " + cliente);
        return cliente;
    }

    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    public Cliente agregarCliente(Cliente cliente) {
        clienteService.agregarCliente(cliente);
        System.out.println("Cliente agregado: " + cliente);
        return cliente;

    }

    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response modificarCliente(@PathParam("id") int id, Cliente clienteModificado) {
        try {
            Cliente cliente = clienteService.encontrarCliente(new Cliente(id));
            if (cliente != null) {
                clienteService.modificarCliente(clienteModificado);
                System.out.println("Cliente modificado: " + clienteModificado);
                return Response.ok().entity(clienteModificado).build();
            } else {
                System.out.println("Cliente no encontrado, no pudo ser modificado");                
                return Response.status(Status.NOT_FOUND).build();
            }

        } catch(Exception e){
            e.printStackTrace(System.out);
            System.out.println("Error interno, el cliente no pudo ser modificado");
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }

    }
    
    @DELETE
    @Path("{id}")
    public Response eliminarCliente(@PathParam("id") int id){
        try{
            clienteService.eliminarCliente(new Cliente(id));
            System.out.println("Cliente eliminado con Id: " + id );
            return Response.ok().build();
        } catch(Exception e){
            e.printStackTrace(System.out);
            System.out.println("Error interno, el cliente no pudo ser eliminado");
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
        
    }
    

}
