import { FlashMessagesService } from 'angular2-flash-messages';
import { ClienteService } from './../../services/cliente-service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/model/cliente.model';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  cliente: Cliente;
  idCliente: number;

  nombreInput: string;
  apellidoInput: string;
  saldoInput: number;
  emailInput: string;
  telefonoInput: string;

  constructor(private clienteService: ClienteService,
              private router: Router,
              private route:ActivatedRoute,
              private flashMessages: FlashMessagesService) { }

  ngOnInit(): void {
    this.idCliente = this.route.snapshot.params['idCliente'];
    if(this.idCliente != null){
      this.cliente = this.clienteService.encontrarCliente(this.idCliente);
      if(this.cliente != null){
        this.nombreInput = this.cliente.nombre;
        this.apellidoInput = this.cliente.apellido;
        this.saldoInput = this.cliente.saldo;
        this.emailInput = this.cliente.email;
        this.telefonoInput = this.cliente.telefono;
      }
    }
  }

  modificarCliente({value, valid}: {value: Cliente, valid: boolean}){
    if(!valid){
      this.flashMessages.show('Por favor, completar el formulario correctamente', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else{
      value.idCliente = this.idCliente;
      this.clienteService.modificarCliente(this.idCliente, value);
      this.router.navigate(['/']);
    }

  }

  eliminarCliente(){
    if(confirm('¿Seguro que desea eliminar el cliente?')){
      this.clienteService.eliminarCliente(this.idCliente);
      this.router.navigate(['/']);
    }
  }

  

}
