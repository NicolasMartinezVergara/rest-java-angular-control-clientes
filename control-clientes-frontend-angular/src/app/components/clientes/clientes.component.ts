import { Cliente } from './../../model/cliente.model';
import { ClienteService } from './../../services/cliente-service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  cliente: Cliente;
  nombreInput: string;
  apellidoInput: string;
  emailInput: string;
  telefonoInput: string;
  saldoInput: number;

  @ViewChild("clienteForm") clienteForm: NgForm;
  @ViewChild("botonCerrar") botonCerrar: ElementRef;

  constructor(private clienteService: ClienteService,
              private flashMessages: FlashMessagesService ) { }

  ngOnInit(): void {
    this.clienteService.obtenerClientes()
    .subscribe(
      (clientesRecibidos: Cliente[]) =>{
        this.clientes = clientesRecibidos;
        this.clienteService.setClientes(this.clientes);
        console.log('Clientes obtenidos del subscriber: ' + this.clientes);

      }
    );

  }

  getSaldoTotal(){
    let saldoTotal: number = 0;
    if(this.clientes != null) {
      this.clientes.forEach(cliente => {
        saldoTotal += cliente.saldo;
      })
    }
    return saldoTotal;

  }

  agregar({value, valid}: {value: Cliente, valid: boolean}){
    if(!valid){
      this.flashMessages.show('Por favor, completar el formulario correctamente', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else{
      this.clienteService.agregarCliente(value);
      this.clienteForm.resetForm();
      this.cerrarModal();

    }
  }

  private cerrarModal(){
    this.botonCerrar.nativeElement.click();
  }

  }
