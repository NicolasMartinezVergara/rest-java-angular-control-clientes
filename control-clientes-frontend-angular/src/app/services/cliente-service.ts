import { Injectable } from "@angular/core";
import { Cliente } from "../model/cliente.model";
import { DataService } from "./data-service";

@Injectable()
export class ClienteService {

  clientes: Cliente[] = [];

  constructor(private dataService: DataService) {}

  setClientes(clientes: Cliente[]){
    this.clientes = clientes;
  }

  obtenerClientes(){
    return this.dataService.cargarClientes();
  }

  agregarCliente(cliente: Cliente){
    console.log('Cliente a agregar: ' + cliente.nombre + cliente.apellido);
    this.dataService.agregarCliente(cliente)
    .subscribe(
      (cliente: Cliente) => {
        this.clientes.push(cliente);
        console.log('Cliente recién agregado al arreglo local: ' + cliente.idCliente + cliente.nombre + cliente.apellido);
      }
    );
  }

  encontrarCliente(idCliente: number) {
    const clienteEncontrado: Cliente = this.clientes.find(cliente => cliente.idCliente == idCliente);
    console.log('Cliente encontrado: ' + clienteEncontrado.idCliente + clienteEncontrado.nombre + clienteEncontrado.apellido);
    return clienteEncontrado;
  }

  modificarCliente(idCliente: number, cliente: Cliente){
    console.log('Cliente a modificar: ' + cliente.idCliente + cliente.nombre + cliente.apellido);
    const clienteModificado: Cliente = this.clientes.find(cliente => cliente.idCliente == idCliente);
    clienteModificado.idCliente = cliente.idCliente;
    clienteModificado.nombre = cliente.nombre;
    clienteModificado.apellido = cliente.apellido;
    clienteModificado.email = cliente.email;
    clienteModificado.telefono = cliente.telefono;
    clienteModificado.saldo = cliente.saldo;
    this.dataService.modificarCliente(idCliente, cliente);

  }

  eliminarCliente(idCliente: number) {
    console.log('Id del cliente a eliminar: ' + idCliente);
    const index = this.clientes.findIndex(cliente => cliente.idCliente == idCliente);
    this.clientes.splice(index, 1);
    this.dataService.eliminarCliente(idCliente);
  }



}
