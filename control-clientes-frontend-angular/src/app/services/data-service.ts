import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Cliente } from "../model/cliente.model";

@Injectable()
export class DataService {

  urlBase = "http://localhost:8080/control-clientes-backend-java/webservice/clientes";

  constructor(private httpClient: HttpClient) {}

  cargarClientes(){
    return this.httpClient.get(this.urlBase);
  }

  agregarCliente(cliente: Cliente) {
    return this.httpClient.post(this.urlBase, cliente);
  }

  modificarCliente(idCliente: number, cliente: Cliente) {
    let url: string;
    url = this.urlBase + '/' + idCliente;
    this.httpClient.put(url, cliente)
    .subscribe(
      (response) => {
        console.log('Resultado de modificar cliente: ' + response);
      },
      (error) => ('Error en modificar cliente: ' + error)
    );
  }

  eliminarCliente(idCliente: number) {
    let url: string;
    url = this.urlBase + '/' + idCliente;
    this.httpClient.delete(url)
    .subscribe(
      (response) => {
        console.log('Resultado de eliminar cliente: ' + response);
      },
      (error) => console.log('Error en eliminar cliente: ' + error)
    );
  }

}
