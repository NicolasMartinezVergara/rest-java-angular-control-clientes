import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: TableroComponent},
  {path: 'clientes/editar/:idCliente', component: EditarClienteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
