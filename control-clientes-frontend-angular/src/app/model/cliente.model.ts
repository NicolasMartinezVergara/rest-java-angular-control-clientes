export class Cliente {

  constructor(public idCliente: number, public nombre: string, public apellido: string, public email: string, public telefono: string, public saldo: number) {}
}
