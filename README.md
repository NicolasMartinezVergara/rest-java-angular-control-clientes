
El objetivo de este proyecto es realizar una aplicación web para gestionar un sistema control de clientes, que permita saber cuántos
clientes tiene un negocio y, además, si adeudan algún saldo (para ser sumado en un saldo total adeudado). 
Estos registros se pueden mostrar, modificar y eliminar, además de agregar por supuesto.
La idea fue combinar la creación del backend con Java EE aplicando el concepto de patrones de diseño para dividir cada capa de la aplicación web,
también patrones de diseño dao. 
Y exponiendo además los métodos de la clase de negocio como servicios web de tipo RESTfull, y así conectar a nuestro backend con nuestro cliente Angular en el frontend.
La conexión a MySQL utiliza JTA para la persistencia de datos, y EJB´s (Enterprise Java Beans - clases de tipo Stateless) para las capas de negocio y dao, de esta
manera pueden inyectar dependencias y contextos de persistencia.

Backend:
Desarrollado con Apache Netbeans, utilizando Java Empresarial (JavaEE version 8), y JDK 1.8.
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
MySQL en localhost:3306 .
Capa de datos utilizando JTA.
Conexión de tipo REST por medio de la librería jersey-client de Glassfish Jersey Core.
Todas estas dependencias del proyecto descargadas mediante la configuración del archivo pom.xml.
El servidor de aplicaciones es Glassfish, versión 5.0.
Servidor en localhost:8080 .

Frontend:
Cliente creado con Angular CLI versión 7.1.4
Servidor de Angular en localhost:4200

Backend:
La configuración de conexión a la base de datos está realizada en el archivo persistence.xml (rest-java-angular-control-clientes/
control-clientes-backend-java/src/main/resources/META-INF/persistence.xml), declarando también la unidad de persistencia como "ClientePU", 
y el jta-data-source (para encontrar el JNDI al pool de conexiones creado, todo desde el servidor de Glassfish)

La clase de dominio o modelo está definida como @Entity para que JTA pueda relacionarla con los registros de la base de datos.
Implementa la interface Serializable y se declara la constante "serialVersionUID = 1L;" para que este objeto pueda ser trasladado por todas las
capas hasta la página del cliente.
Cada objeto tiene su atributo id (o llave primaria de acceso), que será creada de manera automática y autoincrementable desde la base de datos 
por cada registro creado (de ahí la anotación @GeneratedValue en el atributo de Id).
Se generan los métodos "get" y "set" de cada atributo, más el método toString.
Tiene esta clase también un @NamedQuery (con JPQL en lugar de puro SQL) para realizar una propia sentencia de encontrar clientes (un SELECT).


En la capa de datos, dentro de la ruta rest-java-angular-control-clientes/control-clientes-backend-java/src/main/java/ar/com/controlclientes/data, 
se encuentran todas las clases DAO de cada entidad.
En este caso sólo se define la interface de ClienteDAO y la clase que implementa esta interface.
En la clase ClienteDAOImpl se encuentran los métodos para interactuar con la base de datos: get(mediante el método getResultList
del objeto Query - el named query declarado en la clase Cliente), persist (para insertar un nuevo registro), merge (para actualizar un registro) y delete (realizando
primero un merge del objeto para sincronizarlo con la base de datos, y luego delete).
Son las opraciones principales de Create, Read, Update y Delete (CRUD).
Al ser declarada esta clase como un EJB con la anotación @Stateless, permite inyectar la unidad de persistencia de la base de datos (@PersistenceContext),
y de esta forma inicializar nuestro atributo de Entity Manager para acceder a los métodos que reciben y envían información con la base de datos.
El uso de EJB permite además un mejor manejo transaccional.


Siguiendo con la capa de negocio en la ruta rest-java-angular-control-clientes/control-clientes-backend-java/src/main/java/ar/com/controlclientes/service, 
se encuentra la interface de ClienteService más su implementación (respetando el concepto de alta cohesión y bajo acoplamiento entre componentes).
En la interface se define la firma de los métodos.
En la clase de implementación, se implementa la interface y se define con la anotación @Stateless para ser considerada un EJB y así poder inyectar una instancia de 
nuestra clase ClienteDao (la implementación de esta interface), mediante la anotación de @Inject, y así poder acceder a sus métodos.

Creo luego la clase de ClienteServiceRS (en el paquete de restfullwebservice), para realizar nuestra API de tipo RESTfull, exponiendo
los métodos de la capa de negocio.
En primer lugar se agrega la anotación de @Path("") indicando el string con el cual se podrá ingresar por url (en este caso, "/clientes").
Agregamos la anotación @Stateless para poder inyectar una instancia de nuestra clase ClienteServiceImpl, mediante la anotación de @Inject, 
y así poder acceder a sus métodos, los cuales serán llamados dentro de los métodos de esta API.
Por cada método, se antepone el tipo de petición HTTP que representa ese método por medio de anotaciones (@GET, @POST, @PUT, @DELETE),
así como también especificar por medio de las anotaciones de @Consumes y @Produces el tipo de información que consume y produce este web
service (puede ser de tipo XML y/o de tipo JSON, dependiendo el caso).
Para este caso, con Angular se consume y produce información de tipo JSON.

Para configurar el servlet de Jersey, puede hacerse por medio de una clase Java o por medio del archivo web.xml.
En este caso, fue realizado mediante clases Java. Para la capa web, se crean dos clases.
La clase CorsFilter que implementa la interface ContainerResponseFilter (más la anotación de @Provided sobre esta clase), y por medio de su 
método filter configuramos los cabeceros de respuesta del responseContext.
Y la clase JAXRSActivator, que extiende de la clase Application, es la encargada de activar el acceso al web service, declarando en la 
anotación @ApplicationPath("") el string con el cual se podrá ingresar por url (en este caso, "webservice", y luego el path de "/clientes" 
correspondiente a la clase del web service).



Frontend:

Primero en el archivo angular.json se configuran las propiedades de "styles" y "scipts" para el manejo de Bootstrap (previa instalación
con npm install - de Bootstrap, jquery y popper.js -).

La clase Cliente en la ruta control-clientes-frontend-angular/src/app/model/cliente.model.ts, definimos nuestro objeto de modelo/dominio/entidad con sus atributos.

La clase DataService nos permite conectarnos a nuestro web service, utilizando los métodos get, post, put y delete para acceder a los métodos de nuestra API.
Se encuentra en la ruta rest-java-angular-control-clientes/control-clientes-frontend-angular/src/app/services/data-service.ts.
Se declara primero la variable urlBase con el string para localizar nuestro web service ubicado en el backend.
En el caso de los métodos modificarCliente() y eliminarCliente() nos suscribimos a la respuesta del servidor, recibiendo el objeto
de tipo Response en caso de éxito (configurado en el método de nuestra API), o en caso de error.

Dentro del mismo paquete se encuentra la clase ClienteService, encargada de administrar el arreglo local de clientes, es decir, 
el que maneja Angular, una vez recuperada la información desde el DataService. 
Siempre se intenta sincronizar los registros y cada vez que se modifica uno de ellos, se actualiza la base de datos, pero también debe 
actualizarse el arreglo local que maneja Angular, con los nuevos datos para la vista del cliente. Por eso se inicializa un atributo de tipo array con objetos de tipo Cliente.
Se encuentra el método setClientes(), cuya función será sincronizar nuestros listados (esta función recibe un array de clientes, y será
mandada a llamar desde el componente de Clientes, ya que es este componente quien se suscribirá a la respuesta del servidor - a la lista de Clientes 
proveniente de la basede datos).
En esta clase se inyecta una instancia de la clase DataService para mandar a llamar sus métodos en cada uno de los métodos de esta clase enviando los
parámetros a la capa de datos.
Por cada método de esta clase además se modifica el arreglo local de Clientes (sobre el cual trabaja Angular) dependiendo del caso, sea
modificar, elimiar o agregar (manejándolo como un array de esta clase).
En el caso de agregarPersona nos suscribimos a la respuesta del servidor, quien estará enviando un objeto de tipo Cliente (es el cliente
recién agregado a la base de datos, para poder también agregarlo al arreglo local). Como este objeto viene de la base de datos y al insertarlo
también se ejecuta el método "flush()" (dentro del método createCliente en Dao), y con esto, ya se recibirá con su Id generado por la base de datos.

Dentro del paquete de components en la ruta rest-java-angular-control-clientes/control-clientes-frontend-angular/src/app/components, se 
encuentran los componentes que formarán parte de nuesto SPA (Single Page Application), divididos en secciones.
Se definen primero los componentes de cabecero y pie-página, que serán de uso común en todas las vistas de la aplicación, con barra
de navegación y footer. En el componente html de app component, se definen los tags de estos componentes y, entre ellos, el tag de
router-outlet, que nos permitirá navegar en nuestras páginas según la configuración de las rutas del archivo app-routing.module, 
(también por medio del atributo routerLink en el HTML o por medio del método navigate() del objeto Router en nuestras clases de los componentes (.ts)

El componente de tablero será un contenedor para el componente de clientes.

El componente de clientes se encarga primero de suscribirse a la respuesta del servidor del método get de nuestro DataService, recibiendo
así el listado de clientes de la base de datos. Luego, llamará al método setClientes() de la capa de ClienteService y así actualizar también
el arreglo local de clientes. Todo esto dentro del método ngOnInit() de este componente, permitiendo realizar todas estas acciones al 
momento en que se cargue este componente de manera inmediata.
En el componente html, por medio de la directiva *ngFor se itera el listado recibido, y utilizando interpolación agregamos los atributos
de cada objeto sobre el html. Además se realiza el método getSaldoTotal() para sumar todos los saldos de los clientes iterados de la lista.
También se agrega un elemento de clase "card" de Bootstrap para realizar la interpolación {{clientes.length}} y obtener el total de clientes.
Cada cliente iterado tendrá también un botón para redirigir a "editar-cliente", enviando el id de este cliente por url como parámetro ('clientes/editar/:idCliente').

Este componente también se encarga de la función de agregar un nuevo cliente, utilizando una ventana modal que se desplegará sobre la 
pantalla con un formulario de ingreso (el botón para nuestra ventana modal está en el cabecero de este componente, con el target #agregarClienteModal).
En la parte inferior del componente cliente html, se encuentra el formulario modal con el id "agregarClienteModal".
En el componente ts declaro los atributos que corresponderan a cada elemento del formulario - los Inputs - (utilizando Two Way Binding, para el manejo de
los ngForms).
Este formulario cuenta con validación para cada uno de los campos (todos requeridos), y dependiendo el caso, validación de tipo email o de
tipo "mínimo de caracteres requerido" (minlength).
Al hacer submit, se llamará al método agregar() y se envía como parámetro el objeto de tipo ngForm (definido como #clienteForm en el 
html y también en el componente ts para declararlo como atributo utilizando la anotación de @ViewChild).
En este método se procesa el formulario y los valores (value: Cliente, valid: boolean), y si la validación es correcta, se enviará el 
objeto a la clase de servicio. Si no es válido el formulario, no se enviará la información a la clase de servicio, y se enviará un mensaje al hmtl por medio del objeto 
FlashMessages, solicitando completar el formulario correctamente.
También se hará un reset del formulario para que se vacíe luego de agregar al cliente, y se ejecutará el método para cerrar la ventana modal.

El componente de editar-cliente tendrá también su formulario con los campos requeridos (con validaciones) y la acción para enviar la información a ClienteService.
Dentro del componente ts, declaro los atributos del formulario
en el método ngOnInit, primero se almacena el parámetro recibido en el url (en este caso, el parámetro de "id" del cliente).
Utilizando este id, se llama al método encontrarCliente() de nuestro ClienteService, y nos regresa el cliente respectivo del arreglo local de clientes.
Declaramos los atributos que corresponderan a cada elemento del formulario, y por medio de Two Way Binding le asignamos a cada atributo
el valor de los atributos del objeto recuperado, para que el cliente ya pueda visualizar en pantalla los datos y modifique lo que necesite.
Este formulario también cuenta con validaciones campo por campo, como el de agregar cliente.
Al hacer submit, se llama al método modificarCliente que enviará el objeto a ClienteService, con su respectivo id, para luego
redirigirnos a la página principal del listado.

En este componente también pueden eliminarse registros, mediante el botón eliminar que también tendrá disponible nuestro usuario cuando quiera
editar un cliente.
Esto hará una llamada al método eliminarCliente, que ejecutará el método de eliminar de la clase ClienteService, enviándole como parámetro
el id del cliente que se está ejecutando en este momento.
